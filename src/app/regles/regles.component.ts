import { Component, OnInit } from '@angular/core';
import { questions } from '../data/questions';

@Component({
    selector: 'app-regles',
    templateUrl: './regles.component.html',
    styleUrls: ['./regles.component.scss'],
})
export class ReglesComponent implements OnInit {

    public elementsReponse = questions;

    constructor() {
    }

    ngOnInit(): void {
    }
}
