import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridList, MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { MatSidenavModule } from '@angular/material/sidenav';

import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { AppComponent } from './app.component';
import { DialogContentImageComponent } from './image-viewer/dialog-content-image/dialog-content-image.component';
import { MenuComponent } from './menu/menu.component';
import { PlateauComponent } from './plateau/plateau.component';
import { PopinNouvellePartieComponent } from './popin-nouvelle-partie/popin-nouvelle-partie.component';
import { ReglesComponent } from './regles/regles.component';
import { jeuReducer } from './state/jeu.reducer';
import { DesComponent } from './des/des.component';

@NgModule({
    declarations: [
        AppComponent,
        PlateauComponent,
        MenuComponent,
        PopinNouvellePartieComponent,
        ReglesComponent,
        DialogContentImageComponent,
        DesComponent,
    ],
    imports: [
        BrowserModule,
        MatButtonModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatTabsModule,
        MatSidenavModule,
        MatDialogModule,
        MatFormFieldModule,
        MatGridListModule,
        MatInputModule,
        MatSelectModule,
        MatTableModule,
        MatIconModule,
        StoreModule.forRoot({jeu: jeuReducer}),
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {
}
