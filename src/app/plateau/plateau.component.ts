import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Joueur } from '../models/partie.model';
import { augmenterScore, diminuerScore } from '../state/jeu.action';
import { selectJoueurEnCours, selectJoueurs } from '../state/jeu.selectors';
import { PartieService } from '../services/partie.service';

@Component({
  selector: 'app-plateau',
  templateUrl: './plateau.component.html',
  styleUrls: ['./plateau.component.scss']
})
export class PlateauComponent {

  public joueurEnCours$ = this.store.pipe(select(selectJoueurEnCours));
  public joueurs$ = this.store.pipe(select(selectJoueurs));

  displayedColumns: string[] = ['nom', 'score', 'actions'];

  constructor(private store: Store, private partieService: PartieService) { }

  ajouterPoint(joueur: Joueur) {
    this.store.dispatch(augmenterScore({joueur}))
  }

  retirerPoint(joueur: Joueur) {
    this.store.dispatch(diminuerScore({joueur}))
  }

  changerImage() {
    this.partieService.selectionnerImageRandomDepuisListe();
  }
}
