import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { DialogContentImageComponent } from './image-viewer/dialog-content-image/dialog-content-image.component';
import { imageEnCours } from './state/jeu.selectors';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    title = 'random-image';

    public imageEnCours$ = this.store.pipe(select(imageEnCours));

    constructor(private store: Store, private dialog: MatDialog) {
    }

    public ouvrirImageDialog(image: string): void {
        const dialogRef = this.dialog.open(DialogContentImageComponent, {
            data: image,
            backdropClass: 'mat-dialog-container',
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }
}
